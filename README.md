# Phoebus Container

A Container for Phoebus based on EPICS 7.0.7 - Ubuntu 22.04


## Starting the container
**Phoebus** is running using *X11* support. Make sure to have an X Server running on your local machine before starting the container.

```
xhost +local:docker
```

To start the container, use the following command:
```
docker run --rm --network host --env DISPLAY=$DISPLAY -v="$HOME/.Xauthority:/root/.Xauthority:rw" -v $MEMENTO:/root/.phoebus/ -v ./gui:/opt/phoebus/gui -v /tmp/.X11-unix:/tmp/.X11-unix --device=/dev/dri:/dev/dri registry.hzdr.de/hzb/epics/phoebus-container:latest 
```
Alternitavely you can use the example bash script and start it with 

```
./start.sh
```

If you set the environment variable `PHOEBUS_SCREENS_DIR` to some directory this directory will be mounted. If you don't set it then the directory `./gui` in this folder will be mounted. You can put your screens here.

If you set the environment variable `MEMENTO` to some directory this directory will be mounted. If you don't set it then the directory .phoebus will be craeted and mounted. This will ensure persistance of the user settings. 

You can opptionally include other arguments like

* `-list` to show all options available for startup and exit,
* `-resource` pv://?root:aiExample\&root:ai2\&app=databrowser to open the databrowser with the specified PVs,
* `-settings /storage/phoebus_settings.ini` to start with a custom (e.g. bind-mounted) settings file.

For other options see [https://control-system-studio.readthedocs.io/en/latest/running.html](https://control-system-studio.readthedocs.io/en/latest/running.html)

## Opening Screens

This image doesn't contain any screens, but you can place them in the directory `./gui` and they will be mounted. You can then navigate to this directory which is mounted at `/opt/phoebus/gui` and open your screens. 

## Building the container
Simply run

```
docker build -t registry.hzdr.de/hzb/epics/phoebus-container:latest .
```

Then to push it to the registry. You will need to login to the registry first (`docker login ...`): 

```
docker push registry.hzdr.de/hzb/epics/phoebus-container:latest
```

