# Start with the OpenJDK 19 slim-buster base image
FROM openjdk:19-slim-buster AS base

# Update system's timezone
ENV TZ=Europe/Berlin

# Update package lists
RUN apt update

# Update package lists again and install necessary packages:
# - wget: for downloading files from the internet
# - unzip: to extract the downloaded zip file
# - dbus: to support session bus communication, needed by some graphical applications
# - libopenjfx-jni: provides JavaFX JNI bindings, necessary for graphical Java applications
# The --fix-missing option helps handle any missing packages gracefully
# Install required packages
RUN apt-get update \
    && apt-get install -y wget unzip dbus dbus-x11 libopenjfx-jni \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Download the latest Phoebus package and extract it
RUN cd /opt/ && wget --no-check-certificate --continue https://controlssoftware.sns.ornl.gov/css_phoebus/nightly/phoebus-linux.zip
RUN cd /opt/ && unzip phoebus-linux.zip && rm phoebus-linux.zip

# Rename the extracted Phoebus directory to a stable name (/opt/phoebus),
# then rename the JAR file to a consistent name (phoebus.jar) for easier referencing in ENTRYPOINT
RUN mv /opt/phoebus-* /opt/phoebus \
    && cd /opt/phoebus \
    && mv *.jar phoebus.jar

# Set the working directory to the Phoebus installation directory
WORKDIR /opt/phoebus

# Set the display variable to use display 0 (useful if running with a virtual display server like Xvfb)
ENV DISPLAY :0

# Define the entry point to start Phoebus:
# - Launches the JAR file (phoebus.jar) with the Java command
# - Uses `-server` flag for specifying the server port (4918)
ENTRYPOINT ["java", "-jar", "/opt/phoebus/phoebus.jar", "-server", "4918"]
